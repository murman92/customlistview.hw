package com.example.gabriellamurman.listviewhw;

import android.content.ClipData;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

import static android.R.id.list;

public class MainActivity extends AppCompatActivity /*implements AdapterView.OnItemSelectedListener*/ {

    Spinner spinnerCoche;
    public ListView list;
    public ArrayList<Item> items;
    public ItemAdapter adapter;
//"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n" +


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("RENTAL PROPERTIES");

        list = (ListView) findViewById(R.id.lista);

        items = new ArrayList<Item>();
        items.add(new Item("House 1", R.drawable.pic_1, "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod", "3 bathrooms 2 bedrooms", "€3000"));
        items.add(new Item("House 2", R.drawable.pic_2, "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod", "3 bathrooms 2 bedrooms", "€3000"));
        items.add(new Item("House 3", R.drawable.pic_3, "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod", "3 bathrooms 2 bedrooms", "€3000"));
        items.add(new Item("House 4", R.drawable.pic_4, "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod", "3 bathrooms 2 bedrooms", "€3000"));
        items.add(new Item("House 5", R.drawable.pic_1, "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod", "3 bathrooms 2 bedrooms", "€3000"));



        adapter = new ItemAdapter(getApplicationContext(),items);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {

                Item item = (Item) list.getAdapter().getItem(position);

                String namLabel = item.name;
                Log.d("PrincipalActivity: ",item.name);
            }
        });

    }

}
