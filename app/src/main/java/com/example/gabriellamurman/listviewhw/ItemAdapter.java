package com.example.gabriellamurman.listviewhw;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ItemAdapter extends BaseAdapter {

    private Context context;
    private List<Item> items;

    public ItemAdapter(Context context, List<Item> items) {
        this.context = context;
        this.items = items;
    }


    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = convertView;


        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_item, parent, false);
        }

        /* ITEM */
        Item item = this.items.get(position);

        // findViewByIds


        ImageView house = (ImageView) rowView.findViewById(R.id.house);
        TextView name = (TextView) rowView.findViewById(R.id.name);
        TextView  description = (TextView) rowView.findViewById(R.id.description);
        TextView price = (TextView) rowView.findViewById(R.id.price);
        //TextView details = (TextView) rowView.findViewById(R.id.details);


        house.setImageResource(item.imagepictures);
        name.setText(item.name);
        description.setText(item.description);
        price.setText(item.price + "\n" + item.details);
        //details.setText(item.details);

        return rowView;
    }
}


