package com.example.gabriellamurman.listviewhw;


public class Item {

    public String name;
    public String description;
    public int imagepictures;
    public String price;
    public String details;

    public Item(String name, int imagepictures, String description, String details, String price){
        this.name = name;
        this.description = description;
        this.imagepictures = imagepictures;
        this.price = price;
        this.details = details;
    }
}
